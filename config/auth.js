module.exports = {
    'facebookAuth': {
        'clientID': 'your-secret-clientID-here', 
        'clientSecret': 'your-client-secret-here', 
        'callbackURL': 'http://localhost:8080/auth/facebook/callback'
    },

    'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth': {
        'clientID': process.env.GPLUS_ID,
        'clientSecret': process.env.GPLUS_SECRET,
        'callbackURL': 'http://localhost:3000/auth/google/callback'
    }
};