const http = require('http')
const path = require('path')


const socketio = require('socket.io')
const passport = require('passport')
const express = require('express')
const router = express()
const session = require('express-session');
const server = http.createServer(router)

require('dotenv').config()
const io = socketio.listen(server)
const Language = require('@google-cloud/language')


router.use(express.static(path.resolve(__dirname, 'client')))

require('./config/passport')(passport)

// required for passport
var sessionMiddleware = session({
    secret: 'thisisalongmessagetoemulateasecrethash', // session secret
    resave: true,
    saveUninitialized: true
})

router.use(sessionMiddleware);
router.use(passport.initialize());
router.use(passport.session()); // persistent login sessions
require('./router')(router, passport)

var sockets = []

io.use(function (socket, next) {
    // Wrap the express middleware
    sessionMiddleware(socket.request, {}, next);
}).on('connection', function (socket) {
    if (socket.request.session.passport) {
        var user = socket.request.session.passport;
        if (user.user)
            socket.emit('login', user)
        else
            socket.emit('logout')
    }
    sockets.push(socket)
    socket.on('disconnect', function () { })

    socket.on('parseNL', function (text, evt) {
        const data = analyzeText(text)
        for (let item in data) {
            data[item].then(res => {
                socket.emit('parsedNL', { [item]: res })
            })
        }
    })
})

function analyzeText(text) {

    /**
    * Create a V1 languageServiceClient with additional helpers for common tasks.
        Provides text analysis operations such as sentiment analysis and entity recognition.
        @constructor
        @alias module:language
    */
    const language = Language();

    const document = {
        'content': text,
        type: 'PLAIN_TEXT'
    };

    return {
        sentiment: language.analyzeSentiment({ document: document }),
        entities: language.analyzeEntities({ document: document }),
        syntax: language.analyzeSyntax({ document: document })
    }
}

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function () {
    var addr = server.address()
    console.log("Chat server listening at", addr.address + ":" + addr.port)
})